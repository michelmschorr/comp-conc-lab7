//recurso compartilhado entre as threads
class VetorInfos {
    private int vetor[], numPares=0;

    public int tamVetor;


    public VetorInfos(int vetor[]) { 
        this.vetor = vetor; 
        this.tamVetor = vetor.length;
    }

    public synchronized void checaPar(int pos){
        if(vetor[pos]%2==0){
            this.numPares++;
        }
    }

    public synchronized int get(){
        return this.numPares;
    }
}







//tarefa das threads
class ThreadChecadora extends Thread{
    private int id, nthreads;
    private int size;   //tam do bloco
    private VetorInfos vetorInfos;

    public ThreadChecadora(int id, int nthreads, int size, VetorInfos vetorInfos){
        this.id = id;
        this.nthreads = nthreads;
        this.size = size;
        this.vetorInfos = vetorInfos;
    }

    //main das threads
    public void run(){
        final int start = id*this.size;
        final int end;

        if(this.id == nthreads-1){
            end = this.vetorInfos.tamVetor;
        }else{
            end = (id+1)*(this.size);
        }

        for(int i = start; i<end; i++){
            vetorInfos.checaPar(i);
        }
    }
}





//programa
class NumPares {

    static final int nthreads = 2;
    static final int tamVetor = 1000000;
    static final int sizeBlock = tamVetor/nthreads;
    private static int numParesSeq = 0;

    public static void main (String[] args) {
        Thread[] threads = new Thread[nthreads];

        int[] vetor = new int[tamVetor];
        for (int i = 0; i < tamVetor; i++){
            vetor[i] = i;
        }

        VetorInfos vetorInfos = new VetorInfos(vetor);
        

        for (int i = 0; i < threads.length; i++){
            threads[i] = new ThreadChecadora(i, nthreads, sizeBlock, vetorInfos);

        }

        for (int i=0; i<threads.length; i++){
            threads[i].start();
        }

        for (int i = 0; i<threads.length; i++){
            try {threads[i].join(); } catch (InterruptedException e) {return; }
        }


        //checa a quantidade de números pares dentro do vetor[] de maneira sequencial
        for(int i = 0; i < tamVetor; i++){
            if(vetor[i]%2==0)
                numParesSeq++;
        }
        
        //teste de corretude
        if (numParesSeq == vetorInfos.get()){
            System.out.println("O teste de corretude passou!");
            System.out.println("Total de numeros pares no vetor = " + vetorInfos.get());
        } else {
            System.out.println("O teste de corritude não passou!\n");
        }






    }
}
